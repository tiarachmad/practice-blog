<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Session;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$articles = Article::orderBy('id', 'desc')->paginate(10);
        return view ('pages.article')->withArticles($articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $this->validate($request, array (
        	 'title' => 'required|max:255',
        	 'author' => 'required|max:255',
        	 'category_id' => 'required',
        	 'description' => 'required'
        ));

        //store data to database
        $create = new Article;
        $create->title = $request->title;
        $create->author = $request->author;
        $create->category_id = $request->category_id;
        $create->description = $request->description;

        $create->save();

        Session::flash('success', 'Article is successfully created.');

        //redirect to another page
        return redirect()->route('article.show', $create->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$article = Article::find($id);
        return view('pages.detail')->withArticle($article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // find the post in database and save as variable
        $article = Article::find($id);

        // return the view and pass in the variable we previously created
        return view('pages.edit')->withArticle($article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate the data
        $this->validate($request, array (
        	 'title' => 'required|max:255',
        	 'author' => 'required|max:255',
        	 'category_id' => 'required',
        	 'description' => 'required'
        ));

        // save data to database
        $update = Article::find($id);
        $update->title = $request->input('title');
        $update->author = $request->input('author');
        $update->category_id = $request->input('category_id');
        $update->description = $request->input('description');
        $update->save();

        // set flash data with success message
        Session::flash('success', 'Article is successfully saved.');
        
        // redirect with flash data to article.show
        return redirect()->route('article.show', $update->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Article::find($id);
        $delete->delete();
        Session::flash('success', 'Article is successfully deleted.');
        return redirect()->route('article.index');
    }
}
