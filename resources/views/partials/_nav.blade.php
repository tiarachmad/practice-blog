<header>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center light">SIMPLE BLOG</h1>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <a href="/article" class="btn btn-link center-block active">READ ARTICLES</a>
            </div>
            <div class="col-md-6 col-sm-6">
                <a href="/article/create" class="btn btn-link center-block">WRITE ARTICLE</a>
            </div>
        </div>
        <hr>`
    </div>
</header>