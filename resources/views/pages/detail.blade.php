@extends('main')
@section('title', '| Read Article')
@section('content')
	<div class="container">
		<div class="row">
			<h2>{{ $article->title }}</h2>
		</div>
		<div class="row">
			<h5>{{ $article->author }}</h5>
			<h5>{{ date('M - j - y, h:i a', strtotime($article->updated_at)) }}</h5>
			<p>{{ $article->description }}</p>
		</div>
		<br>
		<div class="row">
			{!! Form::open(['route' => ['article.destroy', $article->id], 'method' => 'DELETE']) !!}
			{!! Html::linkRoute('article.edit', 'edit this article', array($article->id)) !!}
			{{ Form::submit('delete this article', array('class' => 'btn btn-default btn-danger btn-sm pull-right')) }}
			{!! Form::close() !!}
		</div>
		<hr>
		<!-- <div class="row">
			<div class="well">
				<dl class="dl-horizontal">
					<dt>Created At :</dt>
					<dd>time</dd>
				</dl>
				<dl class="dl-horizontal">
					<dt>Last Updated :</dt>
					<dd>time</dd>
				</dl>
				<hr>
				<div class="row">
					<div class="col-sm-6">
						<a href="#" class="btn btn-default btn-block">Edit</a>
					</div>
					<div class="col-sm-6">
						<a href="#" class="btn btn-default btn-block">Delete</a>
					</div>
				</div>
			</div>
		</div> -->
	</div>
@endsection