@extends('main')
@section('title', '| Write Article')
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <h3 class="text-center">Write your thought</h3>
                <br>
                {!! Form::open(['route' => 'article.store']) !!}
                    {{ Form::label('title', 'Title :') }}
                    {{ Form::text('title', null, array('class' => 'form-control', 'style' => 'margin-bottom: 1em')) }}

                    {{ Form::label('author', 'Author :') }}
                    {{ Form::text('author', null, array('class' => 'form-control', 'style' => 'margin-bottom: 1em')) }}
                    
                    {{ Form::label('category_id', 'Category :') }}
                    {{ Form::text('category_id', 1, array('class' => 'form-control', 'style' => 'margin-bottom: 1em')) }}

                    {{ Form::label('description', 'Your Article :') }}
                    {{ Form::textarea('description', null, array('class' => 'form-control', 'rows' => '13')) }}

                    {{ Form::submit('SUBMIT', array('class' => 'btn btn-block btn-default btn-lg', 'style' => 'margin-top: 1.5em')) }}
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection

<!-- <div class="row">
    <h3 class="text-center">Write your thought</h3>
    <br>
    <div class="col">
        <form>
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" class="form-control" id="title"></textarea>
            </div>
        </form>
    </div>      
</div>
<div class="row">
    <div class="col">
        <form>
            <div class="form-group">
                <label for="author">Author:</label>
                <input type="text" class="form-control" id="author"></textarea>
            </div>
        </form>
    </div> 
</div>
<div class="row">
    <form>
        <div class="form-group">
            <label for="description">Your Article</label>
            <textarea class="form-control" rows="13" id="description"></textarea>
        </div>
    </form>
</div>
<div class="row">
    <button class="btn btn-block btn-default btn-lg">SUBMIT TO YOUR BLOG</button>
</div> -->