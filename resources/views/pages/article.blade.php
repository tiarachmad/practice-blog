@extends('main')
@section('content')
    <section>
        <div class="container">
            @foreach ($articles as $article)
                <div class="row">
                <div class="col-md-12">
                    <h2>{{ $article->title }}</h2>
                    <h5>{{ $article->author }}, Category</h5>
                    <h5>{{ date('M - j - y, h:i a', strtotime($article->updated_at)) }}</h5>
                    <p class="text-justify">
                        {{ substr($article->description, 0, 500) }}{{ strlen($article->description) > 500 ? "..." : "" }}
                    </p>
                    {!! Html::linkRoute('article.edit', 'edit this article', array($article->id)) !!}
                    {!! Html::linkRoute('article.show', 'read more', array($article->id), array('class' => 'pull-right')) !!}
                    <br>
                    <hr>
                </div>
            </div>
            @endforeach
            <div class="row">
                <div class="text-center pagination">
                    {!! $articles->links(); !!}
                </div>
            </div>
        </div>
    </section>
@endsection