@extends('main')
@section('title', '| Edit Article')
@section('content')
    <section>
     {!! Form::model($article, ['route' => ['article.update', $article->id], 'method' => 'PUT']) !!}
        <div class="container">
            <h3>Edit your article</h3>
            <div class="row">
                {{ Form::label('title', 'Title :') }}
                {{ Form::text('title', null, array('class' => 'form-control', 'style' => 'margin-bottom: 1em')) }}
                {{ Form::label('author', 'Author :') }}
                {{ Form::text('author', null, array('class' => 'form-control', 'style' => 'margin-bottom: 1em')) }}
                
                {{ Form::label('category_id', 'Category :') }}
                {{ Form::text('category_id', 1, array('class' => 'form-control', 'style' => 'margin-bottom: 1em')) }}
                {{ Form::label('description', 'Your Article :') }}
                {{ Form::textarea('description', null, array('class' => 'form-control', 'rows' => '13')) }}
            </div>
            <br>
                {!! Html::linkRoute('article.show', 'cancel', array($article->id)) !!}
                {{ Form::submit('save changes', array('class' => 'btn btn-default btn-sm pull-right')) }}
            <hr>
        </div>
     {!! Form::close() !!}
    </section>
@endsection