<!DOCTYPE html>
<html lang="en">
    @include('partials._head')
    <body style="font-family: 'Raleway">
        @include('partials._nav')

        @include('partials._messages')
        
        @yield('content')
        
        @include('partials._js')
    </body>
</html>